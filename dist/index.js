"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * A forEach method for both Array and NodeList
 * @author Todd Motto
 * @url    https://toddmotto.com/ditch-the-array-foreach-call-nodelist-hack/
 * @param  {array|nodelist}   array    The array/nodelit to loop over
 * @param  {function}         callback The callback function to execute on each item
 * @param  {object}           scope    The scope to execute the callback function in
 * @return {void}
 */
var forEach = exports.forEach = function forEach(array, callback, scope) {
  var length = array.length;
  for (var i = 0; i < length; i++) {
    // passes back stuff we need
    callback.call(scope, array[i], i);
  }
};

exports.default = forEach;