/**
 * A forEach method for both Array and NodeList
 * @author Todd Motto
 * @url    https://toddmotto.com/ditch-the-array-foreach-call-nodelist-hack/
 * @param  {array|nodelist}   array    The array/nodelit to loop over
 * @param  {function}         callback The callback function to execute on each item
 * @param  {object}           scope    The scope to execute the callback function in
 * @return {void}
 */
export const forEach = (array, callback, scope) => {
	const length = array.length
	for (let i = 0; i < length; i++) {
		// passes back stuff we need
		callback.call(scope, array[i], i)
	}
}

export default forEach